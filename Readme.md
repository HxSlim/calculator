# Simple Calculator
This is a simple command line calculator that accepts input in the form:

```
[first operand] [operator] [second operand]

Example:
89 + 90
100 - 45
```

### Supported Operations:

* Addition
* Multiplication
* Subtraction
* Division


