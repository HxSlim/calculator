package com.slim.calculator.compute;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SubtractionOperatorTest {
    private final SubtractionOperator subtractionOperator = new SubtractionOperator();

    @Test
    public void shouldSubtractTwoOperands() {
        //Given
        BigDecimal firstOperand = new BigDecimal(700L);
        BigDecimal secondOperand = new BigDecimal(500L);
        BigDecimal expectedResult = new BigDecimal(200L);
        //When
        BigDecimal actualResult = subtractionOperator.compute(firstOperand, secondOperand);
        //Then
        assertEquals(expectedResult, actualResult);
    }
}