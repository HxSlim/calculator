package com.slim.calculator.compute;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class OperatorFactoryTest {
    private final OperatorFactory operatorFactory = new OperatorFactory();

    @ParameterizedTest(name = "Should create operator instance for {0}")
    @MethodSource("provideSupportedOperatorsArguments")
    public void shouldCreateOperatorInstance_GivenSupportedOperator(String operator, Class<? extends Operator> operatorClass)
            throws UnsupportedOperatorException {
        //When
        Operator operatorInstance = operatorFactory.createOperator(operator);
        //Then
        assertNotNull(operatorInstance);
        assertInstanceOf(operatorClass, operatorInstance);
    }

    @Test
    public void shouldThrowException_GivenUnsupportedOperator() {
        assertThrows(UnsupportedOperatorException.class, () -> operatorFactory.createOperator("operator"));
    }

    private static Stream<Arguments> provideSupportedOperatorsArguments() {
        return Stream.of(
                Arguments.of("+", AdditionOperator.class),
                Arguments.of("-", SubtractionOperator.class),
                Arguments.of("*", MultiplicationOperator.class),
                Arguments.of("/", DivisionOperator.class)
        );
    }
}