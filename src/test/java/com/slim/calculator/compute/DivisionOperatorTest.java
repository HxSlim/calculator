package com.slim.calculator.compute;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DivisionOperatorTest {

    private final DivisionOperator divisionOperator = new DivisionOperator();

    @Test
    public void shouldDivideTwoOperands() {
        //Given
        BigDecimal firstOperand = new BigDecimal(600L);
        BigDecimal secondOperand = new BigDecimal(300L);
        BigDecimal expectedResult = new BigDecimal(2L);
        //When
        BigDecimal actualResult = divisionOperator.compute(firstOperand, secondOperand);
        //Then
        assertEquals(expectedResult, actualResult);
    }
}