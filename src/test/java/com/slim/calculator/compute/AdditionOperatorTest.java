package com.slim.calculator.compute;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AdditionOperatorTest {

    private final AdditionOperator additionOperator = new AdditionOperator();

    @Test
    public void shouldAddTwoOperands() {
        //Given
        BigDecimal firstOperand = new BigDecimal(500L);
        BigDecimal secondOperand = new BigDecimal(700L);
        BigDecimal expectedSum = new BigDecimal(1200L);
        //When
        BigDecimal actualSum = additionOperator.compute(firstOperand, secondOperand);
        //Then
        assertEquals(expectedSum, actualSum);
    }

}