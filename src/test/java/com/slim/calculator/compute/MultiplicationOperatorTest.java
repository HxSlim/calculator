package com.slim.calculator.compute;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultiplicationOperatorTest {

    private final MultiplicationOperator multiplicationOperator = new MultiplicationOperator();

    @Test
    public void shouldMultiplyTwoOperands() {
        //Given
        BigDecimal firstOperand = new BigDecimal(700L);
        BigDecimal secondOperand = new BigDecimal(500L);
        BigDecimal expectedResult = new BigDecimal(350000L);
        //When
        BigDecimal actualResult = multiplicationOperator.compute(firstOperand, secondOperand);
        //Then
        assertEquals(expectedResult, actualResult);
    }

}