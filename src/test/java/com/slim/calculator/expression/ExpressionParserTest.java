package com.slim.calculator.expression;

import com.slim.calculator.compute.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ExpressionParserTest {
    private final OperatorFactory operatorFactory = new OperatorFactory();
    private final ExpressionParser expressionParser = new ExpressionParser(operatorFactory);

    @ParameterizedTest(name = "Should parse the expression: {0}")
    @MethodSource("provideValidExpressionsToParse")
    public void shouldParseAValidExpression(String expressionToParse, ParsedExpression expectedParsedExpression) throws InvalidExpressionException {
        //When
        ParsedExpression actualParsedExpression = expressionParser.parseExpression(expressionToParse);
        //Then
        assertNotNull(actualParsedExpression);
        assertEquals(expectedParsedExpression.firstOperand(), actualParsedExpression.firstOperand());
        assertEquals(expectedParsedExpression.secondOperand(), actualParsedExpression.secondOperand());
        assertEquals(expectedParsedExpression.operator().getClass(), actualParsedExpression.operator().getClass());
    }

    @ParameterizedTest(name = "Should throw exception on invalid expression: [{0}]")
    @ValueSource(strings = {"10+5", "a + 10", "10 + b", "10 *  5"})
    public void shouldThrowExceptionGivenAnInvalidExpression(String expressionToParse) {
        assertThrows(InvalidExpressionException.class, () -> expressionParser.parseExpression(expressionToParse));
    }


    private static Stream<Arguments> provideValidExpressionsToParse() {
        return Stream.of(
                Arguments.of("700 + 500", new ParsedExpression(
                        new BigDecimal(700), new BigDecimal(500), new AdditionOperator())),
                Arguments.of("700 - 500", new ParsedExpression(
                        new BigDecimal(700), new BigDecimal(500), new SubtractionOperator())),
                Arguments.of("700 * 500", new ParsedExpression(
                        new BigDecimal(700), new BigDecimal(500), new MultiplicationOperator())),
                Arguments.of("600 / 300", new ParsedExpression(
                        new BigDecimal(600), new BigDecimal(300), new DivisionOperator()))
        );
    }
}