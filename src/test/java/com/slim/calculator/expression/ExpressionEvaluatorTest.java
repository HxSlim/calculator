package com.slim.calculator.expression;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class ExpressionEvaluatorTest {
    @Autowired
    private ExpressionEvaluator expressionEvaluator;

    @ParameterizedTest(name = "Should evaluate: [{0}] and return : [{1}]")
    @MethodSource("provideValidExpressionsToEvaluate")
    public void shouldEvaluateAValidExpression(String expressionToEvaluate, BigDecimal expectedResult) throws InvalidExpressionException {
        //Given and When
        BigDecimal actualResult = expressionEvaluator.evaluateExpression(expressionToEvaluate);
        //Then
        assertEquals(expectedResult, actualResult);
    }

    @ParameterizedTest(name = "Should throw exception on invalid expression: [{0}]")
    @ValueSource(strings = {"10+5", "a + 10", "10 + b", "10 *  5"})
    public void shouldThrowExceptionGivenAnInvalidExpression(String expressionToEvaluate) {
        assertThrows(InvalidExpressionException.class, () -> expressionEvaluator.evaluateExpression(expressionToEvaluate));
    }

    private static Stream<Arguments> provideValidExpressionsToEvaluate() {
        return Stream.of(
                Arguments.of("700 + 500", new BigDecimal(1200)),
                Arguments.of("700 - 500", new BigDecimal(200)),
                Arguments.of("700 * 500", new BigDecimal(350000)),
                Arguments.of("600 / 200", new BigDecimal(3))
        );
    }

    @Configuration
    @ComponentScan(basePackages = {"com.slim.calculator.compute", "com.slim.calculator.expression"})
    static class TestConfig {

    }
}