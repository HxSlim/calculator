package com.slim.calculator.compute;

import java.math.BigDecimal;

public class DivisionOperator implements Operator {
    @Override
    public BigDecimal compute(BigDecimal firstOperand, BigDecimal secondOperand) {
        return firstOperand.divide(secondOperand);
    }
}
