package com.slim.calculator.compute;

import java.math.BigDecimal;

public class MultiplicationOperator implements Operator {
    @Override
    public BigDecimal compute(BigDecimal firstOperand, BigDecimal secondOperand) {
        return firstOperand.multiply(secondOperand);
    }
}
