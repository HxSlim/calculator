package com.slim.calculator.compute;

import java.math.BigDecimal;

public interface Operator {
    BigDecimal compute(BigDecimal firstOperand, BigDecimal secondOperand);
}
