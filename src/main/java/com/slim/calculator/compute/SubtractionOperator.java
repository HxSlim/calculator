package com.slim.calculator.compute;

import java.math.BigDecimal;

public class SubtractionOperator implements Operator {
    @Override
    public BigDecimal compute(BigDecimal firstOperand, BigDecimal secondOperand) {
        return firstOperand.subtract(secondOperand);
    }
}
