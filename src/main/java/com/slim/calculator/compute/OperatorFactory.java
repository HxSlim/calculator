package com.slim.calculator.compute;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class OperatorFactory {
    private static final String ADDITION_OPERATOR = "+";
    private static final String SUBTRACTION_OPERATOR = "-";
    private static final String MULTIPLICATION_OPERATOR = "*";
    private static final String DIVISION_OPERATOR = "/";

    private static final Map<String, Operator> operatorRegistry = Map.of(
            ADDITION_OPERATOR, new AdditionOperator(),
            SUBTRACTION_OPERATOR, new SubtractionOperator(),
            MULTIPLICATION_OPERATOR, new MultiplicationOperator(),
            DIVISION_OPERATOR, new DivisionOperator()
    );

    public Operator createOperator(String operator) throws UnsupportedOperatorException {
        if (!operatorRegistry.containsKey(operator)) throw new UnsupportedOperatorException();
        return operatorRegistry.get(operator);
    }
}
