package com.slim.calculator;

import com.slim.calculator.expression.ExpressionEvaluator;
import com.slim.calculator.expression.InvalidExpressionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

@SpringBootApplication
@Slf4j
public class CalculatorApplication implements CommandLineRunner {
    private final ExpressionEvaluator expressionEvaluator;

    public CalculatorApplication(ExpressionEvaluator expressionEvaluator) {
        this.expressionEvaluator = expressionEvaluator;
    }

    public static void main(String[] args) {
        SpringApplication.run(CalculatorApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Calculator Started");
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));

        boolean quit = false;
        while (!quit) {
            System.out.println("Enter an expression to evaluate it, or q to exit: ");

            final String expression = reader.readLine();
            if (expression != null && !expression.isBlank() && !"q".equals(expression)) {
                try {
                    BigDecimal result = expressionEvaluator.evaluateExpression(expression);

                    System.out.println("Result is: " + result);
                } catch (InvalidExpressionException invalidExpressionException) {
                    System.out.println("**Invalid Expression**");
                }
            } else if ("q".equals(expression)) {
                quit = true;
            }
        }
        log.info("Calculator Finished");
    }
}
