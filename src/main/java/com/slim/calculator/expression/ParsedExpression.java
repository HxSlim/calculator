package com.slim.calculator.expression;

import com.slim.calculator.compute.Operator;

import java.math.BigDecimal;

public record ParsedExpression(BigDecimal firstOperand, BigDecimal secondOperand, Operator operator) {
}
