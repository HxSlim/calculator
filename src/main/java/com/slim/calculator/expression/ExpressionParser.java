package com.slim.calculator.expression;

import com.slim.calculator.compute.Operator;
import com.slim.calculator.compute.OperatorFactory;
import com.slim.calculator.compute.UnsupportedOperatorException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;

@Component
public class ExpressionParser {
    private final OperatorFactory operatorFactory;

    public ExpressionParser(OperatorFactory operatorFactory) {
        this.operatorFactory = operatorFactory;
    }

    public ParsedExpression parseExpression(String expression) throws InvalidExpressionException {
        String expressionToParse = expression.strip();

        int countOfSpaces = StringUtils.countOccurrencesOf(expressionToParse, " ");
        if (countOfSpaces != 2) throw new InvalidExpressionException();

        String[] splitExpression = expressionToParse.split(" ");

        BigDecimal firstOperand;
        BigDecimal secondOperand;
        Operator operator;

        try {
            firstOperand = new BigDecimal(splitExpression[0]);
            operator = operatorFactory.createOperator(splitExpression[1]);
            secondOperand = new BigDecimal(splitExpression[2]);
        } catch (UnsupportedOperatorException | NumberFormatException e) {
            throw new InvalidExpressionException();
        }

        return new ParsedExpression(firstOperand, secondOperand, operator);
    }
}
