package com.slim.calculator.expression;

import com.slim.calculator.compute.Operator;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class ExpressionEvaluator {
    private final ExpressionParser expressionParser;

    public ExpressionEvaluator(ExpressionParser expressionParser) {
        this.expressionParser = expressionParser;
    }

    public BigDecimal evaluateExpression(String expression) throws InvalidExpressionException {
        final ParsedExpression parsedExpression = expressionParser.parseExpression(expression);

        final BigDecimal firstOperand = parsedExpression.firstOperand();
        final BigDecimal secondOperand = parsedExpression.secondOperand();
        final Operator operator = parsedExpression.operator();

        final BigDecimal result = operator.compute(firstOperand, secondOperand);

        return result;
    }
}
